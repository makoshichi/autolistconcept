﻿using AutoList.Frames;
using System;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inform the amount of windows:");
            string input = Console.ReadLine();
            int quantity;

            while (!int.TryParse(input, out quantity))
                input = Console.ReadLine();

            var windows = new AutoListWindows(quantity);

            for (int i = 0; i < quantity; i++)
                AddWindow(windows, i);

            for (int i = 0; i < quantity; i++)
            {
                var window = windows.Windows[i];
                Console.WriteLine($"Parts for window {i + 1}, dimensions {window.InitialDimensions.X},{window.InitialDimensions.Y}:");
                foreach (var part in window.Parts)
                {
                    Console.WriteLine($"Part type { part.PartId.ToString() }");
                    Console.WriteLine($"Length: { part.Length }");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }

        static void AddWindow(AutoListWindows windows, int index)
        {
            Console.WriteLine($"Inform window {index + 1} dimensions separated by comma only (no spaces)");
            string input = Console.ReadLine();
            string[] dimensions;

            try
            {
                dimensions = input.Split(',');
            }
            catch (Exception)
            {
                AddWindow(windows, index);
                return;
            }

            double x, y;

            if (!double.TryParse(dimensions[0], out x) || !double.TryParse(dimensions[1], out y))
            {
                AddWindow(windows, index);
                return;
            }

            windows.Add(x, y);
        }
    }
}

﻿using AutoList.Parts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoList.Frames
{
    internal interface IFrame
    {
        bool IsNarrow { get; set; }
        Dimension InitialDimensions { get; set; } // Size of the hole on the wall
        List<BasePart> Parts { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoList.Frames
{
    public class AutoListWindows
    {
        public List<Window> Windows { get; }
        public int Quantity { get { return Windows.Count; } }

        public AutoListWindows(int quantity)
        {
            Windows = new List<Window>(quantity);
        }

        public void Add(double x, double y)
        {
            Windows.Add(new Window(new Dimension { X = x, Y = y }));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoList.Parts;

namespace AutoList.Frames
{
    public class Window : IFrame // Window implements IFrame so we can add a "Door" class easier later on
    {
        public Dimension InitialDimensions { get; set; }
        public List<BasePart> Parts { get; set; }
        public bool IsNarrow { get; set; } // In the future will be used to adjust parts calculations. It worries me that in the overview we have nothing regarding position of doors/windows, only size

        public Window(Dimension initialDimensions)
        {
            InitialDimensions = initialDimensions;
            SetParts();
        }

        // This actually looks like very bad implementation and that won't scale, I'll figure something better later on (probably use a custom list for parts)
        private void SetParts()
        {
            var sidePanelLeft = new VerticalPanel(InitialDimensions, PartId.LeftPanel, Constants.THICKNESS);
            double sidePanelWidth = sidePanelLeft.Width; // This Width (see comment in BasePart) is messing up calculations for all horizontal parts

            Parts = new List<BasePart>()
            {
                sidePanelLeft,
                new VerticalPanel(InitialDimensions, PartId.RightPanel, Constants.THICKNESS), 
                new HorizontalPanel(InitialDimensions, PartId.TopPanel, sidePanelWidth),
                new HorizontalPanel(InitialDimensions, PartId.BottomPanel, sidePanelWidth),
                new RainBoard(InitialDimensions, PartId.TopRainBoard, sidePanelWidth),
                new RainBoard(InitialDimensions, PartId.BottomRainBoard, sidePanelWidth)
            };
        }
    }
}

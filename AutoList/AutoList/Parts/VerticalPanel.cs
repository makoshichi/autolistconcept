﻿using System;

namespace AutoList.Parts
{
    public class VerticalPanel : BasePart
    {
        private int thickness;

        public VerticalPanel(Dimension initialDimensions, PartId partId, int thickness) : base(initialDimensions, partId)
        {
            this.thickness = thickness;
        }

        private double Slant {
            get
            {
                double thicknessAsDouble;
                if (double.TryParse(thickness.ToString(), out thicknessAsDouble))
                    return thicknessAsDouble * Math.Tan(Constants.SLANT_ANGLE.ConvertToRadians()); // Math.Tan returns a very unlikely value;
                else
                    throw new Exception("Conversion error in Slant between int and double"); // This is likely to never be reached, but it will help us to debug should the worst happen
            }
        }

        public override double Length // I find a bit weird Length being calculated using the Y axis. Is that correct?
        {
            get
            {
                return initialDimensions.Y - fitting - windowFrame + Slant;
            }
        }
    }
}

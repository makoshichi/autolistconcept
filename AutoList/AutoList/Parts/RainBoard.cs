﻿namespace AutoList.Parts
{
    public class RainBoard : HorizontalPanel
    {
        public RainBoard(Dimension initialDimensions, PartId partId, double sidePanelWidth) 
            : base(initialDimensions, partId, sidePanelWidth) { }

        public override double Length
        {
            get
            {
                return initialDimensions.X - fitting - (2 * windowFrame) + (2 * sidePanelWidth);
            }
        }
    }
}

﻿namespace AutoList.Parts
{
    public class HorizontalPanel : BasePart
    {
        //protected double sidePanelLeftWidth;
        //protected double sidePanelRightWidth;
        protected double sidePanelWidth;
        private int offset = Constants.OFFSET;

        public HorizontalPanel(Dimension initialDimensions, PartId partId, double sidePanelWidth) : base(initialDimensions, partId)
        {
            this.sidePanelWidth = sidePanelWidth;
        }

        public override double Length
        {
            get
            {
                return initialDimensions.X - fitting - (2 * windowFrame) + (2 * sidePanelWidth) + offset; // Left and right surely are the same width, aren't they?
            }
        }
    }
}

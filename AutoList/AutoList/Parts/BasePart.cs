﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoList.Parts
{
    public abstract class BasePart
    {
        protected int fitting; // Reduction from the whole in the wall to the used window dimensions
        protected int windowFrame; // Maybe we should find a better name for it; reduction because sidepanels over lap the window
        protected Dimension initialDimensions;

        public PartId PartId { get; set; }

        public virtual double Length { get; } // Length will be calculated by de deriving classes
        public virtual double Width { get { return initialDimensions.X; } } // This just isn't right, but there's nothing in the overview indicating what is the input or formula for a part's width

        public BasePart(Dimension initialDimensions, PartId partId)
        {
            this.PartId = partId;
            this.initialDimensions = initialDimensions;
            fitting = Constants.FITTING;
            windowFrame = Constants.WINDOW_FRAME; // temporary usage, must find out how to calculate windowFrame from sidepanels width
        }
    }
}

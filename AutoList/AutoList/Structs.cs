﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoList
{
    public struct Dimension
    {
        public double X; // In milimeters; slant being calculated by tan(75) demands datatype to be double instead of int thus avoiding data loss
        public double Y; // In milimeters
    }
}

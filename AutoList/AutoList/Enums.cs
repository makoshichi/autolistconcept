﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoList
{
    public enum PartId
    {
        TopRainBoard = 1,
        TopPanel = 2,
        BottomRainBoard = 3,
        BottomPanel = 4,
        LeftPanel = 5,
        RightPanel = 6
    }
}

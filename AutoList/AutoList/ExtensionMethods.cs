﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoList
{
    public static class ExtensionMethods
    {
        public static double ConvertToRadians(this double angle)
        {
            return (Math.PI / 180) * angle;
        }
    }
}

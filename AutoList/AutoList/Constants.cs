﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoList
{
    class Constants
    {
        public static readonly int FITTING = 10; // In milimeters
        public static readonly int OFFSET = 40; // In milimeters
        public static readonly double SLANT_ANGLE = 75; // In degrees

        // Unclear to what is the actual input to these values, fetched from these constants for the moment 
        public static readonly int WINDOW_FRAME = 15; // This is temporary
        public static readonly int THICKNESS = 10; // This is temporary
    }
}
